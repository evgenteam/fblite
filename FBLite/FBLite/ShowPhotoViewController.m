//
//  ShowPhotoViewController.m
//  FBLite
//
//  Created by Alexey Babichev on 24.02.13.
//  Copyright (c) 2013 Evgen Matsiyuk. All rights reserved.
//

#import "ShowPhotoViewController.h"

@interface ShowPhotoViewController ()<UIScrollViewDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIPinchGestureRecognizer *pinchGestureRecognizer;
@end

@implementation ShowPhotoViewController
@synthesize imageView = _imageView;
@synthesize scrollView = _scrollView;
@synthesize pinchGestureRecognizer = _pinchGestureRecognizer;


- (void)setImageView
{
    UIImage *myImage = [UIImage imageNamed:@"apriliars125.png"];
    _imageView = [[UIImageView alloc] initWithImage:myImage];  
    [self.view addSubview:_imageView];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
//    _scroll.contentSize = CGSizeMake(830.0f , 840.0f);
//    [_scroll setContentSize:CGSizeMake(_imageView.frame.size.width, _imageView.frame.size.height)];
    self.scrollView.minimumZoomScale=0.5;
    
    self.scrollView.maximumZoomScale=6.0;
    
    self.scrollView.contentSize=CGSizeMake(1280, 960);
    
    self.scrollView.delegate=self;
    [self setImageView];
}
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView

{
    
    return self.imageView;
    
}
- (void)viewDidUnload
{
    _imageView = nil;
    
    [super viewDidUnload];
    
}


@end
