//
//  ViewController.h
//  FBLite
//
//  Created by Alexey Babichev on 22.02.13.
//  Copyright (c) 2013 Evgen Matsiyuk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "SCViewController.h"
#import "ShowPhotoViewController.h"


@interface ViewController : UIViewController

@end
