//
//  ViewController.m
//  FBLite
//
//  Created by Alexey Babichev on 22.02.13.
//  Copyright (c) 2013 Evgen Matsiyuk. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()<FBFriendPickerDelegate, FBUserSettingsDelegate>

@property (nonatomic, strong) FBFriendPickerViewController *friendPickerController;
@property (nonatomic, strong) FBUserSettingsViewController *userSettingsViewController;
@property (nonatomic, strong) SCViewController *scViewController;
@property (nonatomic, strong) ShowPhotoViewController *showPhotoViewController;

@property (nonatomic, strong) FBRequest *request;
@property (strong, nonatomic) NSArray* selectedFriends;
@end

@implementation ViewController
@synthesize userSettingsViewController = _userSettingsViewController;
@synthesize scViewController = _scViewController;
@synthesize friendPickerController = _friendPickerController;
@synthesize showPhotoViewController = _showPhotoViewController;
@synthesize request = _request;
@synthesize selectedFriends =_selectedFriends;

#pragma mark - life cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}
- (IBAction)logIn:(id)sender
{
    _userSettingsViewController = [[FBUserSettingsViewController alloc] initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:self.userSettingsViewController animated:YES];
    

    
            if (![[FBSession activeSession] isOpen])
           
            {
                NSArray *permissions = [[NSArray alloc] initWithObjects:@"email",@"publish_stream",@"user_birthday", nil];
    
                [FBSession openActiveSessionWithPermissions:permissions
                                                      allowLoginUI:YES
                                                 completionHandler:^(FBSession *session, FBSessionState status, NSError *error){
    
                                                 }];
    }
}

- (IBAction)logOut:(id)sender
{
    
    _showPhotoViewController = [[ShowPhotoViewController alloc] initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:self.showPhotoViewController animated:YES];
//    [self logOut];
//    FBRequest* friendsRequest = [FBRequest requestForMyFriends];
//    [friendsRequest startWithCompletionHandler: ^(FBRequestConnection *connection,
//                                                  NSDictionary *result,
//                                                  NSError *error) {
//        NSArray *friends = [result objectForKey:@"data"];
//        NSLog(@"Found: %@ friends", result);
//        for (NSDictionary<FBGraphUser>* friend in friends) {
//            NSLog(@"I have a friend named %@ with id %@", friend.name, friend.id);
//        }
//    }];
}

- (IBAction)fbFriendList:(id)sender
{
    if ([[FBSession activeSession] isOpen])
    {
    [self.friendPickerController loadData];
    [self.navigationController pushViewController:self.friendPickerController animated:YES];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please Log In facebook"
                                                        message:nil
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}
 

    
- (FBFriendPickerViewController *)friendPickerController
{
    if(!_friendPickerController)
    {
        _friendPickerController = [[FBFriendPickerViewController alloc] initWithNibName:nil bundle:nil];
        _friendPickerController.delegate = self;
    }

    return _friendPickerController;
    }

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 2:
            if (!self.friendPickerController) {
                self.friendPickerController = [[FBFriendPickerViewController alloc]
                                               initWithNibName:nil bundle:nil];
                
                // Set the friend picker delegate
                self.friendPickerController.delegate = self;
                
                self.friendPickerController.title = @"Select friends";
            }
            
            [self.friendPickerController loadData];
            [self.navigationController pushViewController:self.friendPickerController animated:YES];
            break;
    }
}

//- (void)updateCellIndex:(int)index withSubtitle:(NSString*)subtitle {
//    UITableViewCell *cell = (UITableViewCell *)[self.menuTableView
//                                                cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
//    cell.detailTextLabel.text = subtitle;
//}

- (void)updateSelections
{
    
    NSString* friendsSubtitle = @"Select friends";
    int friendCount = self.selectedFriends.count;
    if (friendCount > 2) {
        // Just to mix things up, don't always show the first friend.
        id<FBGraphUser> randomFriend =
        [self.selectedFriends objectAtIndex:arc4random() % friendCount];
        friendsSubtitle = [NSString stringWithFormat:@"%@ and %d others",
                           randomFriend.name,
                           friendCount - 1];
    } else if (friendCount == 2) {
        id<FBGraphUser> friend1 = [self.selectedFriends objectAtIndex:0];
        id<FBGraphUser> friend2 = [self.selectedFriends objectAtIndex:1];
        friendsSubtitle = [NSString stringWithFormat:@"%@ and %@",
                           friend1.name,
                           friend2.name];
    } else if (friendCount == 1) {
        id<FBGraphUser> friend = [self.selectedFriends objectAtIndex:0];
        friendsSubtitle = friend.name;
    }
//    [self updateCellIndex:2 withSubtitle:friendsSubtitle];
}

- (void)friendPickerViewControllerSelectionDidChange: (FBFriendPickerViewController *)friendPicker
{
    NSArray *friend = friendPicker.selection;
    NSLog(@" %@ ", friend);
    
    [self updateSelections];
    _scViewController = [[SCViewController alloc] initWithNibName:nil bundle:nil];

    [self.navigationController pushViewController:self.scViewController animated:YES];

    
//    [self.placePickerController loadData];
    
//    _placePickerController = [[FBUserSettingsViewController alloc] initWithNibName:nil bundle:nil];
//    
//    [self.navigationController pushViewController:self.placePickerController animated:YES];
}

-(void)logOut
{
    [[FBSession activeSession] closeAndClearTokenInformation];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

@end
