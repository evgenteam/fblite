//
//  SCViewController.m
//  FBLite
//
//  Created by Alexey Babichev on 24.02.13.
//  Copyright (c) 2013 Evgen Matsiyuk. All rights reserved.
//

#import "SCViewController.h"
@interface SCViewController ()
@property (strong, nonatomic) IBOutlet FBProfilePictureView *userProfileImage;
@property (strong, nonatomic) IBOutlet UILabel *userNameLabel;
@property (strong, nonatomic) IBOutlet UITextView *userInfoTextView;
- (void)populateUserDetails;
@end
@implementation SCViewController
@synthesize userProfileImage = _userProfileImage;
@synthesize userNameLabel = _userNameLabel;
@synthesize userInfoTextView = _userInfoTextView;

#pragma mark - life cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (FBSession.activeSession.isOpen)
    {
        [self populateUserDetails];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}

- (void)populateUserDetails
{
    if (FBSession.activeSession.isOpen)
    {
        [[FBRequest requestForMe] startWithCompletionHandler:
         ^(FBRequestConnection *connection,
           NSDictionary<FBGraphUser> *user,
           NSError *error)
        {
             if (!error)
             {
                 NSLog( @" %@ ", user);
                 self.userNameLabel.text = user.name;
                 self.userProfileImage.profileID = user.id;
             }
         }];
    }
}

- (void)sessionStateChanged:(NSNotification*)notification {
    if (FBSession.activeSession.isOpen) {
//        [self.authButton setTitle:@"Logout" forState:UIControlStateNormal];
        self.userInfoTextView.hidden = NO;
    } else {
//        [self.authButton setTitle:@"Login" forState:UIControlStateNormal];
        self.userInfoTextView.hidden = YES;
    }
}
@end
