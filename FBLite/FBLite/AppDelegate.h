//
//  AppDelegate.h
//  FBLite
//
//  Created by Alexey Babichev on 22.02.13.
//  Copyright (c) 2013 Evgen Matsiyuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
extern NSString *const SCSessionStateChangedNotification;

@property (strong, nonatomic) ViewController *viewController;
@property (nonatomic , strong) UINavigationController *navigationController;

@end
